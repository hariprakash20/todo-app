let tasks = []
let task
let newId = 1;
let completed = false;
const tabsContainer = document.getElementById("to-do-tabs")
const tabs = document.querySelectorAll(".to-do-tab")
const form = document.getElementById("to-do-form")
const tasksContainer = document.getElementById("tasks-container")
const deleteAllBtn = document.getElementById("delete-all-btn")
const generateID = () => newId++;

tabsContainer.addEventListener("click", event => {
       tabs.forEach(tab => tab.classList.remove("active-tab"));
       event.target.classList.add("active-tab")
       if(event.target.dataset.id == 1) {
        completed = false;
          printTasks(tasks)
       }
       if(event.target.dataset.id == 2){
        onlyActive()
       } 
       if(event.target.dataset.id == 3){
        onlyCompleted()
       }
})

form.addEventListener("submit", function(event){
    event.preventDefault()
    task = form.task.value
    if(task.length > 0) {
       addTask(task)
       printTasks(tasks)
       form.reset()
    }
})

tasksContainer.addEventListener("click", event =>{
    tasks.forEach(task =>{
        if(task.id==event.target.id){
            task.finish = !task.finsh;
        }
    })
    if(event.target.classList.contains("delete-btn")){
        tasks.forEach(task =>{
            if(task.id==event.target.dataset.id){
                tasks.splice(tasks.indexOf(task), 1);
            }
        })
        onlyCompleted();
    }
})

deleteAllBtn.addEventListener("click", () =>{
    tasks = tasks.filter(function (task) { 
        return !task.finish 
    })
    printTasks([]);
 })



function addTask(task){
    const taskObj = {
       id: generateID(),
       task: task,
       finish: false,
    }
    tasks.push(taskObj);
    printTasks(tasks)  
}

 function printTasks(tasks) {
    tasksContainer.textContent = ""
    tasks.forEach(function (taskObj){
        let id = taskObj.id;
        let task = taskObj.task;
        let finish = taskObj.finish;
       let li = document.createElement("li")
       li.classList.add("task")
       let div = document.createElement("div")
       div.classList.add("task-checkbox")
       div.innerHTML =`
          <input class="task-input" type="checkbox" id="${id}">
          <label  class="task-label"  for="${id}">${task}</label>
       `
       li.appendChild(div)
       const i = document.createElement("I")
       i.classList.add("fa", "fa-trash", "delete-btn","hidden");
       completed ? deleteAllBtn.classList.remove('hidden') : deleteAllBtn.classList.add('hidden');
       completed ? i.classList.remove('hidden') : i.classList.add('hidden');
       if(finish) {
        div.firstElementChild.checked = true
       }
       i.dataset.id = id;
       li.appendChild(i);
       tasksContainer.appendChild(li)
    })
 }

function onlyCompleted(){
    completed = true;
    printTasks(tasks.filter(task => task.finish))
 }
function onlyActive(){
    completed = false;
    printTasks(tasks.filter(task => !task.finish))
 }